'use strict';

const { createCanvas, loadImage, registerFont } = require("canvas");
const { isBrowser } = require("browser-or-node");

if (typeof(registerFont) != "undefined") {
    registerFont(__dirname + (__dirname.endsWith("/") ? "" : "/") + "../assets/fonts/Roboto-Regular.ttf", { family: "Roboto", weight: "normal" });
    registerFont(__dirname + (__dirname.endsWith("/") ? "" : "/") + "../assets/fonts/Roboto-Bold.ttf", { family: "Roboto", weight: "bold" });
}

// Minutes to pixels
const minToPx = (t) =>
{
    return Math.round((64 / 180) * t);
};

const dateToMin = (date) =>
{
    if (typeof(date) !== "string") {
        return -1;
    }

    const m = date.match(/^[0-9]{2,4}-[0-9]{2}-[0-9]{2} ([0-9]{2}):([0-9]{2}):[0-9]{2}/);
    if (m) {
        return parseInt(m[1]) * 60 + parseInt(m[2]);
    } else {
        return -1;
    }
};

const sleepBar = (w, h, fade_in, fade_out, mx, my) =>
{
                                // w - bar width
                                // h - bar height
    mx = mx || 0;               // horizontal margins
    my = my || 0;               // vertical margins
    fade_in = fade_in || 0;     // add fade_in
    fade_out = fade_out || 0;   // add fade_out

    const r = w / 2;            // arc radius
    const cw = r * 2 + mx * 2;  // canvas width
    const ch = h + my * 2;      // canvas height

    const canvas = createCanvas(cw, ch);
    const ctx = canvas.getContext("2d");

    ctx.fillStyle = "#000000";
    ctx.beginPath();
    ctx.moveTo(0 + mx, r + my);
    ctx.arc(r + mx, r + my, r, Math.PI, 2 * Math.PI);
    ctx.lineTo(r * 2 + mx, h - r + my - 1);
    ctx.arc(r + mx, h - r + my - 1, r, 2 * Math.PI, Math.PI);
    ctx.lineTo(0 + mx, r + my);
    ctx.fill();

    if (fade_in || fade_out) {
        ctx.globalCompositeOperation = "destination-out";
        let gradient = ctx.createLinearGradient(0, 0, 0, ch);
        if (fade_in && fade_out) {
            gradient.addColorStop(0, "rgba(255, 255, 255, 0.98)");
            gradient.addColorStop(0.5, "rgba(255, 255, 255, 0)");
            gradient.addColorStop(1, "rgba(255, 255, 255, 0.98)");
        } else if (fade_in) {
            gradient.addColorStop(0, "rgba(255, 255, 255, 0.98)");
            gradient.addColorStop(0.5, "rgba(255, 255, 255, 0)");
        } else if (fade_out) {
            gradient.addColorStop(0.5, "rgba(255, 255, 255, 0)");
            gradient.addColorStop(1, "rgba(255, 255, 255, 0.98)");
        }
        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, cw, ch);
    }

    return canvas;

};

const iconPath = (icon) =>
{
    if (isBrowser) {
        switch (icon) {
            case "nap":
                return require("../assets/icons/report-nap.png");
            case "wake-full":
                return require("../assets/icons/report-wake-full.png");
                break;
            case "wake-lights":
                return require("../assets/icons/report-wake-lights.png");
                break;
        };
    } else {
        const p = "../assets/icons/report-" + icon + ".png";
        return __dirname + (__dirname.endsWith("/") ? "" : "/") + p;
    }
};

const sleepTable = async (days, clw, h, mx, my) =>
{
    mx = mx || 0;
    my = my || 0;
    const n = days.length;
    const thh = 20;                     // table header height
    const clhw = 40;                    // column header width
    const clfw = 10;                    // column footer width
    const clmy = 16;                    // column vertical margin
    const cw = clhw + (n * clw) + clfw + (2 * mx); // canvas width
    const ch = h + (2 * my);              // canvas height

    const canvas = createCanvas(cw, ch);
    const ctx = canvas.getContext("2d");

    let last_ess = null;
    let ess_start = null;
    ctx.fillStyle = "#607FB6";
    ctx.beginPath();

    for (let i = 0; i < n; i++) {

        let clstart = (i ? clhw : 0) + i * clw + mx;
        let clwidth = clw + (i ? 0 : clhw);
        if (i == n) {
            clwidth = clfw;
        }
        let clend = clstart + clwidth;

        let data;
        if (i < n) {
            data = days[i];
        } else {
            data = days[n - 1];
        }

        let expected_sleep_start = dateToMin(data.expected_sleep_start) - 720;
        if (expected_sleep_start < 0) {
            expected_sleep_start += 1440;
        }
        if (last_ess === null) {
            last_ess = expected_sleep_start;
        }

        let start = { x: clstart - 10, y: thh + clmy + minToPx(last_ess) + my };
        if (start.x < 0) start.x = 0;
        let end =   { x: clstart + 10, y: thh + clmy + minToPx(expected_sleep_start) + my };
        let cp1 =   { x: start.x + (end.x - start.x) / 3 * 1,   y: start.y  };
        let cp2 =   { x: start.x + (end.x - start.x) / 3 * 2,   y: end.y };
        last_ess = expected_sleep_start;

        if (ess_start === null) {
            ess_start = { x: start.x, y: start.y };
            ctx.moveTo(ess_start.x, ess_start.y);
        }

        // Bézier curve
        ctx.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y, end.x, end.y);
        ctx.lineTo(clend - 10, thh + clmy + minToPx(last_ess) + my);

    }

    ctx.lineTo(cw - mx, thh + clmy + minToPx(last_ess) + my);

    last_ess = null;

    for (let i = n; i >= 0; i--) {

        let clstart = (i ? clhw : 0) + i * clw + mx;
        let clwidth = clw + (i ? 0 : clhw);
        if (i == n) {
            clwidth = clfw;
        }
        let clend = clstart + clwidth;

        let data;
        if (i > 0) {
            data = days[i - 1];
        } else {
            data = days[0];
        }

        let expected_sleep_end = (dateToMin(data.expected_sleep_start) + parseInt(data.expected_sleep_time)) - 720;
        if (expected_sleep_end < 0) {
            expected_sleep_end += 1440;
        }

        if (last_ess === null) {
            last_ess = expected_sleep_end;
        }

        if (i == n) {
            ctx.lineTo(cw - mx, thh + clmy + minToPx(last_ess) + my);
            ctx.lineTo(clend - 10, thh + clmy + minToPx(last_ess) + my);
        }

        ctx.lineTo(clstart + 10, thh + clmy + minToPx(last_ess) + my);

        let start = { x: clstart + 10, y: thh + clmy + minToPx(last_ess) + my  };
        let end =   { x: clstart - 10, y: thh + clmy + minToPx(expected_sleep_end) + my };
        if (end.x < 0) end.x = 0;
        let cp1 =   { x: start.x + (end.x - start.x) / 3 * 1,   y: start.y  };
        let cp2 =   { x: start.x + (end.x - start.x) / 3 * 2,   y: end.y  };
        last_ess = expected_sleep_end;

        // Bézier curve
        ctx.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y, end.x, end.y);

    }

    ctx.lineTo(ess_start.x, ess_start.y);

    ctx.fill();

    for (let i = 0; i < (n + 1); i++) {

        let is_light = (i % 2);
        if (is_light) {
            continue;
        }

        let clstart = clhw + i * clw + mx;
        let clwidth = clw;
        if (i == n) {
            clwidth = clfw;
        }

        ctx.fillStyle = "rgba(196, 196, 196, 0.35)";

        ctx.fillRect(
            clstart,
            thh + 0 + my,
            clwidth,
            h
        );

    }

    const day2l = {
        "Monday": "Mo",
        "Tuesday": "Tu",
        "Wednesday": "We",
        "Thursday": "Th",
        "Friday": "Fr",
        "Saturday": "Sa",
        "Sunday": "Su"
    };

    ctx.font = "bold 18px 'Roboto'";
    ctx.textAlign = "center";
    for (let i = 0; i < n; i++) {
        if ((days[i].weekday === "Saturday") || (days[i].weekday === "Sunday")) {
            ctx.fillStyle = "#FF0000";
        } else {
            ctx.fillStyle = "#000000";
        }
        ctx.fillText(day2l[days[i].weekday], clhw + (i * clw) + (clw / 2) - 2, ch - 16);
    }

    ctx.strokeStyle = "#DADADA";
    ctx.font = "bold 16px 'Roboto'";
    ctx.textAlign = "center";
    ctx.fillStyle = "#000000";
    let time = 12;
    for (let i = 0; i < 9; i++) {
        ctx.beginPath();
        ctx.moveTo(clhw - 10 + mx, thh + clmy + minToPx(i * 180) + my);
        ctx.lineTo(cw - 5 - mx, thh + clmy + minToPx(i * 180) + my);
        ctx.stroke();
        let text = "" + (time % 24);
        if (text.length == 1) {
            text = "0" + text;
        }
        time += 3;
        ctx.fillText(text, 15 + mx, thh + clmy + minToPx(i * 180) + 5 + my);
    }

    for (let i = 0; i < n; i++) {

        let data = days[i];
        let sleep_start, sleep_end, sleep_time;
        let fade_in = 0, fade_out = 0;

        if (data.sleep_start) {
            sleep_start = dateToMin(data.sleep_start) - 720;
        } else if (data.expected_sleep_start) {
            sleep_start = dateToMin(data.expected_sleep_start) - 720;
            fade_in = 1;
        } else {
            continue;
        }

        if (sleep_start < 0) {
            sleep_start += 1440;
        }

        if (data.sleep_end) {
            sleep_end = dateToMin(data.sleep_end) - 720;
            if (sleep_end < 0) {
                sleep_end += 1440;
            }
        } else if (data.actual_sleep_time) {
            sleep_time = parseInt(data.actual_sleep_time);
        } else if (data.expected_sleep_time) {
            sleep_time = parseInt(data.expected_sleep_time);
            if (data.sleep_start && data.expected_sleep_start) {
                let sleep_start_diff = (dateToMin(data.sleep_start) - dateToMin(data.expected_sleep_start));
                if (sleep_start_diff < -720) {
                    sleep_start_diff += 1440;
                } else if (sleep_start_diff > 720) {
                    sleep_start_diff -= 1440;
                }
                sleep_time -= sleep_start_diff;
            }
            fade_out = 1;
        }

        if (!sleep_time) {
            if (typeof(sleep_end) != "undefined") {
                sleep_time = sleep_end - sleep_start;
            }
        }

        if (!sleep_time) {
            continue;
        }

        if (sleep_time < 0) {
            sleep_time = sleep_time + 1440;
        }

        let sleep_bar_width = 26;

        ctx.drawImage(
            sleepBar(
                sleep_bar_width,
                minToPx(sleep_time),
                fade_in,
                fade_out
            ),
            clhw + (i * clw) + (clw - sleep_bar_width) / 2,
            thh + clmy + minToPx(sleep_start) + my
        );

        let iconwidth = sleep_bar_width - 6;

        let sleepmodeicon = "wake-full";
        if (typeof(data['flags']) == "object") {
            if (data['flags'].includes("silent")) {
                sleepmodeicon = "wake-lights";
                iconwidth = sleep_bar_width;
            } else if (data['flags'].includes("off")) {
                sleepmodeicon = null;
            }
        }

        if (sleepmodeicon) {
            let expected_sleep_end = (dateToMin(data.expected_sleep_start) + parseInt(data.expected_sleep_time)) - 720;
            if (expected_sleep_end < 0) {
                expected_sleep_end += 1440;
            }
            const icon = await loadImage(iconPath(sleepmodeicon));
            const sleepmodeiconheight = icon.height * (iconwidth / icon.width);
            ctx.drawImage(
                icon,
                clhw + (i * clw) + (clw - sleep_bar_width) / 2 + (sleep_bar_width - iconwidth) / 2,
                thh + clmy + (minToPx(expected_sleep_end) - sleepmodeiconheight - minToPx(30)) + my,
                iconwidth,
                sleepmodeiconheight
            );
        }

        if (data.naps) {
            iconwidth = sleep_bar_width;
            for (let j = 0; j < data.naps.length; j++) {
                let nap_start = dateToMin(data.naps[j].start) -  720;
                if (nap_start < 0) {
                    nap_start += 1440;
                }
                const napicon = await loadImage(iconPath("nap"));
                const napiconheight = napicon.height * (iconwidth / napicon.width);
                ctx.drawImage(
                    napicon,
                    clhw + (i * clw) + (clw - sleep_bar_width) / 2 + (sleep_bar_width - iconwidth) / 2,
                    thh + clmy + minToPx(nap_start) + my,
                    iconwidth,
                    napiconheight
                );
            }
        }
    }

    return canvas;
};

const sleepStats = async (days) =>
{
    const width = 470;
    const height = 706;
    const clw = parseInt((width - 50) / 7); // Column width

    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext("2d");

    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(0, 0, width, height);

    const title = days[0].shortdate + " - " + days[days.length - 1].shortdate;
    ctx.font = "24px 'Roboto'";
    ctx.textAlign = "center";
    ctx.fillStyle = "#000000";
    ctx.fillText(title, width / 2, 48);

    const table = await sleepTable(
        days,
        clw,
        height - 40 - 58
    );

    ctx.drawImage(
        table,
        0,
        clw
    );

    ctx.fillStyle = "#626262";
    ctx.fillRect(1, height - 42, width - 2, 41);

    ctx.font = "bold 17px 'Roboto'";
    ctx.textAlign = "left";
    ctx.fillStyle = "#FFFFFF";
    ctx.fillText("S", 5, height - 16);
    ctx.font = "bold 16px 'Roboto'";
    ctx.fillText("core", 15, height - 16);

    ctx.font = "bold 20px 'Roboto'";
    ctx.textAlign = "center";
    ctx.fillStyle = "#FFFFFF";
    for (let i = 0; i < days.length; i++) {
        if (days[i].score !== null) {
            ctx.fillText(days[i].score, 40 + i * clw + (clw / 2) - 2, height - 14);
        }
    }

    return canvas;
};

module.exports = sleepStats;
