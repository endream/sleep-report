'use strict';

const { createCanvas, loadImage, registerFont } = require("canvas");

if (typeof(registerFont) != "undefined") {
    registerFont(__dirname + (__dirname.endsWith("/") ? "" : "/") + "../assets/fonts/Roboto-Regular.ttf", { family: "Roboto", weight: "normal" });
    registerFont(__dirname + (__dirname.endsWith("/") ? "" : "/") + "../assets/fonts/Roboto-Bold.ttf", { family: "Roboto", weight: "bold" });
}

// Minutes to pixels
const minToPx = (t) =>
{
    return Math.round((64 / 180) * t);
};

const envBorder = (cw, ch, color, conf, fade_in, fade_out) =>
{
                                // cw - bar (canvas) width
                                // ch - bar (canvas) height
    fade_in = fade_in || 0;     // add fade_in
    fade_out = fade_out || 0;   // add fade_out

    const canvas = createCanvas(cw, ch);
    const ctx = canvas.getContext("2d");

    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(cw, 0);
    ctx.lineTo(cw, ch);
    ctx.lineTo(0, ch);
    ctx.lineTo(0, 0);
    ctx.fill();

    if (fade_in || fade_out) {
        ctx.globalCompositeOperation = "destination-out";
        let gradient = ctx.createLinearGradient(0, 0, 0, ch);
        if (fade_in && fade_out) {
            gradient.addColorStop(0, "rgba(255, 255, 255, 0.98)");
            gradient.addColorStop(0.5, "rgba(255, 255, 255, 0)");
            gradient.addColorStop(1, "rgba(255, 255, 255, 0.98)");
        } else if (fade_in) {
            gradient.addColorStop(0, "rgba(255, 255, 255, 0.98)");
            gradient.addColorStop(1, "rgba(255, 255, 255, 0)");
        } else if (fade_out) {
            gradient.addColorStop(0, "rgba(255, 255, 255, 0)");
            gradient.addColorStop(1, "rgba(255, 255, 255, 0.98)");
        }
        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, cw, ch);
    }

    return canvas;

};

const envTable = async (cw, ch, conf, days) =>
{
    const n = days.length;

    const canvas = createCanvas(cw, ch);
    const ctx = canvas.getContext("2d");

    const colors = [
        [ 32, "#FFABB2",  "22°", 32, "#5EADF6",  "16°",  "Temp" ], // Temperature
        [ 32, "#BBBBBB",  "60%", 32, "#F5D642",  "40%",    "RH" ], // Humidity
        [  0,      null, "5lux",  8, "#726ED4",     "", "Light" ], // Lux
    ];

    for (let i = 0; i < colors.length; i++) {

        if (colors[i][6].length) {
            ctx.font = "bold 20px 'Roboto'";
            ctx.textAlign = "left";
            ctx.fillText(colors[i][6], 10, conf.thh + (conf.rh * i) + (conf.rs * i) + conf.rh / 2 + 8);
        }

        ctx.font = "14px 'Roboto'";
        ctx.textAlign = "left";

        if (colors[i][0]) {
            ctx.drawImage(
                envBorder(
                    cw,
                    colors[i][0],
                    colors[i][1],
                    conf,
                    0, 1
                ),
                0,
                conf.thh + (conf.rh * i) + (conf.rs * i)
            );
        }
        if (colors[i][2].length) {
            ctx.fillText(colors[i][2], conf.clhw - 30, conf.thh + (conf.rh * i) + (conf.rs * i) + colors[i][0] + 5);
        }

        if (colors[i][3]) {
            ctx.drawImage(
                envBorder(
                    cw,
                    colors[i][3],
                    colors[i][4],
                    conf,
                    1, 0
                ),
                0,
                conf.thh + (conf.rh * (i + 1)) + (conf.rs * i) - colors[i][3]
            );
        }
        if (colors[i][5].length) {
            ctx.fillText(colors[i][5], conf.clhw - 30, conf.thh + (conf.rh * (i + 1)) + (conf.rs * i) - colors[i][3] + 5);
        }

    }

    for (let i = 0; i < (n + 1); i++) {

        let is_light = (i % 2);
        if (is_light) {
            continue;
        }

        let clstart = conf.clhw + i * conf.clw;
        let clwidth = conf.clw;
        if (i == n) {
            clwidth = conf.clfw;
        }

        ctx.fillStyle = "rgba(196, 196, 196, 0.35)";

        ctx.fillRect(
            clstart,
            conf.thh,
            clwidth,
            ch
        );

    }

    const temp_low = 13.0;
    const temp_high = 25.0;
    const rh_low = 30.0;
    const rh_high = 70.0;
    const lux_low = Math.log(Math.E);
    const lux_high = Math.log(Math.E + 5 * Math.E);

    for (let i = 0; i < n; i++) {
        let day = days[i];
        // Number of data points (excluding the ones which are part of the waking period)
        let pts = parseInt((day.expected_sleep_time - 30) / 10);
        let tstamp = day.timestamp;
        let envpos = 0;
        let val = null;

        ctx.strokeStyle = "#000000";
        ctx.fillStyle = "#000000";
        ctx.lineWidth = 3;

        ctx.beginPath();
        for (let j = 0; j < pts; j++) {
            while (envpos < day.env.length && day.env[envpos].timestamp <= tstamp) {
                val = day.env[envpos].temperature;
                envpos++;
            }
            if (val !== null) {
                val = parseFloat(val);
                if (val < temp_low) {
                    val = temp_low;
                } else if (val > temp_high) {
                    val = temp_high;
                }
                ctx.lineTo(
                    conf.clhw + i * conf.clw + j / (pts - 1) * (conf.clw - 4) + 2,
                    conf.thh + conf.rh - (val - temp_low) / (temp_high - temp_low) * conf.rh
                );
            }
            tstamp += 10 * 60;
        }
        ctx.stroke();

        tstamp = day.timestamp;
        envpos = 0;
        val = null;

        ctx.beginPath();
        for (let j = 0; j < pts; j++) {
            while (envpos < day.env.length && day.env[envpos].timestamp <= tstamp) {
                val = day.env[envpos].rh;
                envpos++;
            }
            if (val !== null) {
                val = parseFloat(val);
                if (val < rh_low) {
                    val = rh_low;
                } else if (val > rh_high) {
                    val = rh_high;
                }
                ctx.lineTo(
                    conf.clhw + i * conf.clw + j / (pts - 1) * (conf.clw - 4) + 2,
                    conf.thh + conf.rh * 2 + conf.rs - (val - rh_low) / (rh_high - rh_low) * conf.rh
                );
            }
            tstamp += 10 * 60;
        }
        ctx.stroke();

        tstamp = day.timestamp;
        envpos = 0;
        val = null;

        ctx.lineWidth = 1;
        ctx.strokeStyle = "#5D91FC";
        ctx.fillStyle = "#5D91FC";

        ctx.beginPath();
        ctx.moveTo(
            conf.clhw + i * conf.clw + 2,
            conf.thh + conf.rh * 3 + conf.rs * 2 - 2
        );
        let y = conf.thh + conf.rh * 3 + conf.rs * 2 - 2;
        for (let j = 0; j < pts; j++) {
            while (envpos < day.env.length && day.env[envpos].timestamp <= tstamp) {
                val = day.env[envpos].lux;
                envpos++;
            }
            if (val !== null) {
                val = parseFloat(val);
                val = Math.log(Math.E + val * Math.E);
                if (val < lux_low) {
                    val = lux_low;
                } else if (val > lux_high) {
                    val = lux_high;
                }
                ctx.lineTo(
                    conf.clhw + i * conf.clw + j / (pts - 1) * (conf.clw - 4) + 2,
                    y
                );
                y = conf.thh + conf.rh * 3 + conf.rs * 2 - (val - lux_low) / (lux_high - lux_low) * conf.rh - 1;
                ctx.lineTo(
                    conf.clhw + i * conf.clw + j / (pts - 1) * (conf.clw - 4) + 2,
                    y
                );
/*
                ctx.fillRect(
                    conf.clhw + i * conf.clw + j / (pts - 1) * (conf.clw - 4) + 2,
                    conf.thh + conf.rh * 3 + conf.rs * 2 - ((val - lux_low) / (lux_high - lux_low) * conf.rh + 1),
                    1,
                    (val - lux_low) / (lux_high - lux_low) * conf.rh + 1
                );
*/
            }
            tstamp += 10 * 60;
        }
        ctx.lineTo(
            conf.clhw + i * conf.clw + conf.clw - 2,
            conf.thh + conf.rh * 3 + conf.rs * 2 - 1
        );
        ctx.lineTo(
            conf.clhw + i * conf.clw + 2,
            conf.thh + conf.rh * 3 + conf.rs * 2 - 1
        );
        ctx.fill();
        ctx.stroke();

    }

    const day2l = {
        "Monday": "Mo",
        "Tuesday": "Tu",
        "Wednesday": "We",
        "Thursday": "Th",
        "Friday": "Fr",
        "Saturday": "Sa",
        "Sunday": "Su"
    };

    ctx.font = "bold " + conf.thfs + "px 'Roboto'";
    ctx.textAlign = "center";
    for (let i = 0; i < n; i++) {
        if ((days[i].weekday === "Saturday") || (days[i].weekday === "Sunday")) {
            ctx.fillStyle = "#FF0000";
        } else {
            ctx.fillStyle = "#000000";
        }
        ctx.fillText(day2l[days[i].weekday], conf.clhw + (i * conf.clw) + (conf.clw / 2) - 2, Math.round(conf.thfs * 0.9));
    }

    return canvas;
};

const envStats = async (days) =>
{
    const width = 597;
    const height = 600;
    const n = days.length;

    const conf = {
        thh:    32,                         // thh - table header height
        thfs:   24,                         // thfw - table header font size
        clw:    parseInt((width - 86) / n), // clw - Column width
        clhw:   86,                         // clhw - column header width (left)
        clfw:    0,                         // clfw - column footer width (right)
        rh:    130,                         // rh - row height
        rs:     20,                         // rs - spacing between rows
    };

    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext("2d");

    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(0, 0, width, height);

    const title = days[0].shortdate + " - " + days[days.length - 1].shortdate;
    ctx.font = "24px 'Roboto'";
    ctx.textAlign = "center";
    ctx.fillStyle = "#000000";
    ctx.fillText(title, width / 2, 48);

    const table = await envTable(
        conf.clhw + (n * conf.clw) + conf.clfw,
        height - 40 - 58,
        conf,
        days
    );

    ctx.drawImage(
        table,
        0,
        72
    );

    ctx.fillStyle = "#626262";
    ctx.fillRect(1, height - 42, width - 2, 41);

    ctx.font = "bold 21px 'Roboto'";
    ctx.textAlign = "left";
    ctx.fillStyle = "#FFFFFF";
    ctx.fillText("S", 10, height - 14);
    ctx.font = "bold 20px 'Roboto'";
    ctx.fillText("core", 22, height - 14);

    ctx.font = "bold 20px 'Roboto'";
    ctx.textAlign = "center";
    ctx.fillStyle = "#FFFFFF";
    for (let i = 0; i < days.length; i++) {
        if (days[i].score !== null) {
            ctx.fillText(days[i].score, conf.clhw + i * conf.clw + (conf.clw / 2), height - 14);
        }
    }

    return canvas;
};

module.exports = envStats;
