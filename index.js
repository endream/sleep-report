'use strict';

const sleepStats = require('./lib/sleep.js')
const envStats = require('./lib/environment.js')

module.exports = {
    sleepStats,
    envStats
};
